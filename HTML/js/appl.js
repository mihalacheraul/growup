///////-------------------------------------F I R E B A S E  C O N N E C T I O N----------------------------------------------
var firebaseConfig = {
    apiKey: "AIzaSyD2NPGPBr7J9xPxADUwHwu01zehGHOtfvM",
    authDomain: "greenlife-4b330.firebaseapp.com",
    databaseURL: "https://greenlife-4b330.firebaseio.com",
    projectId: "greenlife-4b330",
    storageBucket: "greenlife-4b330.appspot.com",
    messagingSenderId: "903375017369",
    appId: "1:903375017369:web:dba4ae19a63460ef54af08",
    measurementId: "G-G29ZF7VF6T"
};
const auth = firebase.auth();
var database = firebase.database();
var refUsers = database.ref("Users");
var refDevices = database.ref("Devices");
var refPlants = database.ref("Flowers");

var userKey = decodeURIComponent(window.location.search).substring(1); //phone number

///////-------------------------------------I N I T----------------------------------------------
var roomName;
var serialNoToBeSet;
var phone;
var FirstName;
var password;
var selectedDeviceIndex;

refUsers.on('value', function(snapshot) { //get the user info from Firebase
    phone = snapshot.val()[userKey]["phone"];
    FirstName = snapshot.val()[userKey]["FirstName"];
    getFirstNameToShow();
});

function getFirstNameToShow() {
    document.getElementById("greetUser").innerHTML = "Hello, " + FirstName + "!";
}


function letsGrowButton() {
    var deviceList = document.getElementById("deviceList");
    let option = document.createElement("option");
    let myKeys = [];

    deviceList.options[deviceList.options.length] = new Option("Choose one device");

    document.getElementById("deviceControl").style.visibility = 'visible';
    document.getElementById("newDevice").style.visibility = 'hidden';
    document.getElementById("settingsMenu").style.visibility = 'hidden';
    document.getElementById("tipsntricksViewer").style.visibility = 'hidden';
    document.getElementById("addNewPlantDiv").style.visibility = 'hidden';
    refDevices.on('value', function(snapshot) {
        let i, L = deviceList.options.length - 1;
        myKeys = [];
        selectedDeviceIndex = deviceList.selectedIndex;
        //create array from all devices for specified owner
        for (var deviceKey in snapshot.val()) {
            if (snapshot.val()[deviceKey]["owner"] == userKey) {
                myKeys.push(deviceKey);
            }
        }
        for (i = L; i >= 0; i--) {
            deviceList.remove(i);
        }
        for (index in myKeys) {
            option.innerHTML = snapshot.val()[myKeys[index]]["flowerName"] + "/" + snapshot.val()[myKeys[index]]["location"];
            option.value = myKeys[index];
            deviceList.options[deviceList.options.length] = new Option(option.innerHTML, option.value);
        }
        deviceList.selectedIndex = selectedDeviceIndex;
        viewPlant();
    });


}
/*
BRIEF:
Function implemented in order to command pump manually
Called on water now button
 */
function manualPumpState() {
    let selectedKey = document.getElementById("deviceList").value;
    let pumpState;
    refDevices.on('value', function(snapshot) {
        pumpState = snapshot.val()[selectedKey]["IrrigationMode"]["ManualButton"];
    });

    if (pumpState == "1") {
        pumpState = "0";
    } else
        pumpState = "1";
    if (pumpState == true) {
        markButton("manualButton", "true");
    } else {
        markButton("manualButton", "false");
    }
    firebase.database().ref('Devices/' + selectedKey + "/IrrigationMode").update({
        ManualButton: pumpState
    });
}

/*
BRIEF:
Function implemented in order to view details about devices in control panel
Called on Control panel
 */
function viewPlant() {
    let selectedKey = document.getElementById("deviceList").value;
    let selectedIrrigationMode = document.getElementById("irrigationMode");
    let temp = document.getElementById('temperature');
    let tankBackground = document.getElementById('circleTank');
    let tankLevel = document.getElementById('lebelTank');
    let soilMoisture = document.getElementById('soilMoisture');
    let soilMoistureText = document.getElementById('soilMoistureText');
    let temperature, humidity, waterLevel, soilMoistureRead, soilMoistureSet;
    let recurrencyQuantity = document.getElementById("recurrenceQuantity");
    let manualQuantity = document.getElementById("manualQuantity");
    let automaticPercent = document.getElementById("automaticQuantity");
    let irrigationMode, RecurrenceTOD;

    refDevices.on('value', function(snapshot) {
        temperature = snapshot.val()[selectedKey]["Air temperature"];
        humidity = snapshot.val()[selectedKey]["Air humidity"];
        waterLevel = snapshot.val()[selectedKey]["Water tank level"];
        soilMoistureRead = snapshot.val()[selectedKey]["Soil moisture"];
        soilMoistureSet = snapshot.val()[selectedKey]["setSoilMoisture"];
        automaticMode = snapshot.val()[selectedKey]["IrrigationMode"]["Automatic"];
        recurrenceMode = snapshot.val()[selectedKey]["IrrigationMode"]["Recurrence"];
        irrigationMode = snapshot.val()[selectedKey]["IrrigationMode"]["IrrigationMode"];

        switch (irrigationMode) {
            case "0":
                document.getElementById("AutomaticMode").style.display = "block";
                document.getElementById("RecurrenceMode").style.display = "none";
                document.getElementById("automaticSetWater").innerHTML = snapshot.val()[selectedKey]["IrrigationMode"]["AutomaticQuantity"];
                automaticPercent.value = snapshot.val()[selectedKey]["IrrigationMode"]["AutomaticQuantity"];
                selectedIrrigationMode.selectedIndex = "0";

                break;
            case "1":
                document.getElementById("RecurrenceMode").style.display = "block";
                document.getElementById("AutomaticMode").style.display = "none";
                recurrencyQuantity.value = snapshot.val()[selectedKey]["IrrigationMode"]["RecurrencyQuantity"];
                document.getElementById("recurrentSetWater").innerHTML = snapshot.val()[selectedKey]["IrrigationMode"]["RecurrencyQuantity"] / 10;
                selectedIrrigationMode.selectedIndex = "1";
                for (let day in snapshot.val()[selectedKey]["weekDays"]) {
                    var dayID = "change" + day;
                    if (snapshot.val()[selectedKey]["weekDays"][day] == "1") {
                        document.getElementById(dayID).style.backgroundColor = "#335313";
                        document.getElementById(dayID).style.color = "yellow";
                        document.getElementById(dayID).style.border = "1px solid yellow";
                    } else {
                        document.getElementById(dayID).style.backgroundColor = "#4d7d1c";
                        document.getElementById(dayID).style.color = "white";
                        document.getElementById(dayID).style.border = "0px solid transparent";
                    }
                }
                RecurrenceTOD = snapshot.val()[selectedKey]["IrrigationMode"]["RecurrenceTOD"];
                switch (RecurrenceTOD) {
                    case "1":
                        markButton("changeMorning", "true");
                        markButton("changeMidday", "false");
                        markButton("changeAfternoon", "false");
                        markButton("changeEvening", "false");
                        break;
                    case "2":
                        markButton("changeMidday", "true");
                        markButton("changeMorning", "false");
                        markButton("changeAfternoon", "false");
                        markButton("changeEvening", "false");
                        break;
                    case "3":
                        markButton("changeAfternoon", "true");
                        markButton("changeMidday", "false");
                        markButton("changeEvening", "false");
                        markButton("changeMorning", "false");
                        break;
                    case "4":
                        markButton("changeEvening", "true");
                        markButton("changeMidday", "false");
                        markButton("changeAfternoon", "false");
                        markButton("changeMorning", "false");
                        break;
                }

                break;
        }

        manualQuantity.value = snapshot.val()[selectedKey]["IrrigationMode"]["ManualQuantity"];
        document.getElementById("manualSetWater").innerHTML = snapshot.val()[selectedKey]["IrrigationMode"]["ManualQuantity"] / 10;
        if (snapshot.val()[selectedKey]["IrrigationMode"]["ManualButton"] == "1") {
            document.getElementById("manualButton").style.backgroundColor = "#335313";
            document.getElementById("manualButton").style.color = "yellow";
            document.getElementById("manualButton").style.border = "1px solid yellow";
        } else {
            document.getElementById("manualButton").style.backgroundColor = "#4d7d1c";
            document.getElementById("manualButton").style.color = "white";
            document.getElementById("manualButton").style.border = "0px solid transparent";
        }

    });

    let tankGradient = waterLevel / 100;
    let moistureGradient = soilMoistureRead / 100;
    if (temperature > 0 && temperature < 50)
        temp.innerText = temperature + "°C";
    tankLevel.innerHTML = waterLevel + "%";
    soilMoistureText.innerHTML = soilMoistureRead + "%";
    tankBackground.style.background = "rgba(0,125,184," + tankGradient + ")";
    soilMoisture.style.background = "rgba(142,91,12," + moistureGradient + ")";
}

// Mark button with specific color - depends on button status 
function markButton(buttonID, status) {
    if (status == "true") {
        document.getElementById(buttonID).style.backgroundColor = "#335313";
        document.getElementById(buttonID).style.color = "yellow";
        document.getElementById(buttonID).style.border = "1px solid yellow";
    } else {
        document.getElementById(buttonID).style.backgroundColor = "#4d7d1c";
        document.getElementById(buttonID).style.color = "white";
        document.getElementById(buttonID).style.border = "0px solid transparent";
    }
}

function changeMoisture() {
    let soilMoistureRangeBar = document.getElementById('setMoisture').value;
    let selectedKey = document.getElementById("deviceList").value;
    let displayedValue = document.getElementById("displayedValue");
    displayedValue.innerHTML = soilMoistureRangeBar + "%";
    firebase.database().ref('Devices/' + selectedKey).update({
        setSoilMoisture: soilMoistureRangeBar
    });

}

///////-------------------------------------N E W   D E V I C E----------------------------------------------
function newDeviceButton() {
    document.getElementById("deviceControl").style.visibility = 'hidden';
    document.getElementById("newDevice").style.visibility = 'visible';
    document.getElementById("settingsMenu").style.visibility = 'hidden';
    document.getElementById("tipsntricksViewer").style.visibility = 'hidden';
    document.getElementById("addNewPlantDiv").style.visibility = 'hidden';
}


function registerNewDevice() {
    serialNoToBeSet = document.getElementById('serialNumber').value; //serial number from Appl
    roomName = document.getElementById('location').value;
    flowerName = document.getElementById('flower').value;
    let responseMessage = document.getElementById('messageLebel');

    if (serialNoToBeSet.length == 8) {
        refDevices.on('value', function(snapshot) {
            for (var deviceKey in snapshot.val()) {
                if (deviceKey == serialNoToBeSet) {
                    if (snapshot.val()[deviceKey]["paired"] == "Unpaired") {
                        //SerialNumber found in DB  
                        firebase.database().ref('Devices/' + deviceKey).update({
                            paired: "paired",
                            location: roomName,
                            flowerName: flowerName,
                            owner: userKey
                        });
                        responseMessage.innerHTML = "**You registered this device successfully!**";
                        return 0;
                    }
                    if (snapshot.val()[deviceKey]["paired"] == "paired") {
                        responseMessage.innerHTML = "**This device is already paired!**";
                        return 0;
                    }

                }
            }
        });
    } else {
        responseMessage.innerHTML = "**The serial number you entered is invalid.**";
    }
}
///////-------------------------------------S E T T I N G S----------------------------------------------
function settingsButton() {
    document.getElementById("deviceControl").style.visibility = 'hidden';
    document.getElementById("newDevice").style.visibility = 'hidden';
    document.getElementById("settingsMenu").style.visibility = 'visible';
    document.getElementById("tipsntricksViewer").style.visibility = 'hidden';
    document.getElementById("addNewPlantDiv").style.visibility = 'hidden';
}

function editDevice() {
    serialNoToBeEdit = document.getElementById('serialNumberEditMenu').value; //serial number from edit meunu
    newRoomName = document.getElementById('relocationLocation').value;
    flower = document.getElementById("relocationPlantName").value;
    let responseMessage = document.getElementById('messageLebelEditMenu');

    refDevices.on('value', function(snapshot) {
        for (var deviceKey in snapshot.val()) {
            if (deviceKey == serialNoToBeEdit) {

                firebase.database().ref('Devices/' + deviceKey).update({
                    location: newRoomName,
                    flowerName: flower
                });
                responseMessage.innerHTML = "**You've edited this device successfully!**";
                return 0;
            }
        }
        responseMessage.innerHTML = "**The serial number you entered is invalid!**";
    });



}



///////-------------------------------------T I P S  A N D  T R I C K S-------------------------------------------
function tipsnTricksShow() {
    document.getElementById("deviceControl").style.visibility = 'hidden';
    document.getElementById("newDevice").style.visibility = 'hidden';
    document.getElementById("settingsMenu").style.visibility = 'hidden';
    document.getElementById("tipsntricksViewer").style.visibility = 'visible';
    document.getElementById("addNewPlantDiv").style.visibility = 'hidden';

    var plantsList = document.getElementById("availablePlantDetalis");
    let option = document.createElement("option");
    let myKeys = [];
    plantsList.options[plantsList.options.length] = new Option("Choose one plant");

    refPlants.on('value', function(snapshot) {;
        let i, L = plantsList.options.length - 1;
        myKeys = [];
        selectedDeviceIndex = plantsList.selectedIndex;
        for (var plantKey in snapshot.val()) {
            myKeys.push(plantKey);
        }
        myKeys = myKeys.sort();

        for (i = L; i >= 0; i--) {
            plantsList.remove(i);
        }
        for (index in myKeys) {
            option.innerHTML = myKeys[index];
            option.value = myKeys[index];
            plantsList.options[plantsList.options.length] = new Option(option.innerHTML, option.value);
        }
        plantsList.selectedIndex = selectedDeviceIndex;
        getDataForPlant();
    });
}


function getDataForPlant() {
    let blooming;
    let details;
    let luminosity;
    let moisture;
    let ambientalTemp;
    let sourceImage;
    let selectedKey = document.getElementById("availablePlantDetalis").value;
    refPlants.on('value', function(snapshot) {
        blooming = snapshot.val()[selectedKey]["Blooming"];
        details = snapshot.val()[selectedKey]["Details"];
        luminosity = snapshot.val()[selectedKey]["Luminosity"];
        moisture = snapshot.val()[selectedKey]["Moisture"];
        ambientalTemp = snapshot.val()[selectedKey]["AmbientalTemp"];
        sourceImage = snapshot.val()[selectedKey]["ImageSource"];
    });
    document.getElementById("plantImage").src = sourceImage;
    document.getElementById("moisturePlant").innerHTML = moisture + "%"
    document.getElementById("temperaturePlant").innerHTML = ambientalTemp + "°C";
    document.getElementById("luminosityPlant").innerHTML = luminosity;
    document.getElementById("bloomingPlant").innerHTML = blooming;
    document.getElementById("detailsPlant").innerHTML = details;
}

/*BRIEF

*/
/*---------------------------------------------------------------------------- */
function changeDayStatus(weekDay, buttonID) {
    let selectedKey = document.getElementById("deviceList").value;
    let changeDay = document.getElementById(buttonID);
    let dayState;
    refDevices.on('value', function(snapshot) {
        dayState = snapshot.val()[selectedKey]['weekDays'][weekDay];
    });
    if (dayState == "1")
        dayState = "0";
    else
        dayState = "1";
    if (dayState == "1") {
        changeDay.style.backgroundColor = "#335313";
        changeDay.style.color = "#ff6600";
        changeDay.style.border = "1px solid #ff6600";
    } else {
        changeDay.style.backgroundColor = "#4d7d1c";
        changeDay.style.color = "white";
        changeDay.style.border = "1px solid transparent";
    }

    switch (weekDay) {
        case 'Monday':
            firebase.database().ref('Devices/' + selectedKey + '/weekDays').update({
                Monday: dayState
            });
            break;
        case 'Tuesday':
            firebase.database().ref('Devices/' + selectedKey + '/weekDays').update({
                Tuesday: dayState
            });
            break;
        case 'Wednesday':
            firebase.database().ref('Devices/' + selectedKey + '/weekDays').update({
                Wednesday: dayState
            });
            break;
        case 'Thursday':
            firebase.database().ref('Devices/' + selectedKey + '/weekDays').update({
                Thursday: dayState
            });
            break;
        case 'Friday':
            firebase.database().ref('Devices/' + selectedKey + '/weekDays').update({
                Friday: dayState
            });
            break;
        case 'Saturday':
            firebase.database().ref('Devices/' + selectedKey + '/weekDays').update({
                Saturday: dayState
            });
            break;
        case 'Sunday':
            firebase.database().ref('Devices/' + selectedKey + '/weekDays').update({
                Sunday: dayState
            });
            break;
    }
}

/*
BRIEF:
Function implemented in order to choose the time of the day for irrigation
Called on recurrence mode

RecurrenceTOD = 1 // morning
                2 // midday
                3 // afternoon
                4 //evening
 */
function changeTimeOfDayStatus(timeOfTheDay, buttonID) {
    let selectedKey = document.getElementById("deviceList").value;
    let changeDay = document.getElementById(buttonID);
    let dayTimeState;


    switch (timeOfTheDay) {
        case 'Morning':
            firebase.database().ref('Devices/' + selectedKey + '/IrrigationMode').update({
                RecurrenceTOD: "1"
            });
            break;
        case 'Midday':
            firebase.database().ref('Devices/' + selectedKey + '/IrrigationMode').update({
                RecurrenceTOD: "2"
            });
            break;
        case 'Afternoon':
            firebase.database().ref('Devices/' + selectedKey + '/IrrigationMode').update({
                RecurrenceTOD: "3"
            });
            break;
        case 'Evening':
            firebase.database().ref('Devices/' + selectedKey + '/IrrigationMode').update({
                RecurrenceTOD: "4"
            });
            break;
    }
}

/*---------------------------------------------------------------------------- */
function setAutomaticQuantity() {
    let pourWater = document.getElementById("automaticQuantity").value;
    let selectedKey = document.getElementById("deviceList").value;
    firebase.database().ref('Devices/' + selectedKey + "/IrrigationMode").update({
        AutomaticQuantity: pourWater
    });
}

function setRcurrencyQuantity() {
    let pourWater = document.getElementById("recurrenceQuantity").value;
    let selectedKey = document.getElementById("deviceList").value;
    pourWater = pourWater;
    firebase.database().ref('Devices/' + selectedKey + "/IrrigationMode").update({
        RecurrencyQuantity: pourWater
    });
}

function setManualQuantity() {
    let pourWater = document.getElementById("manualQuantity").value;
    let selectedKey = document.getElementById("deviceList").value;
    firebase.database().ref('Devices/' + selectedKey + "/IrrigationMode").update({
        ManualQuantity: pourWater
    });
}

function addNewPlant() {
    document.getElementById("tipsntricksViewer").style.visibility = 'hidden';
    document.getElementById("addNewPlantDiv").style.visibility = 'visible';
}

/*
brief:
read new data
process name to make it fit to standard
create and register the new plant
*/
function newPlantRegistration() {
    let name = document.getElementById("newPlantName").value;
    let moisture = document.getElementById("newPlantMoisture").value;
    let ambTemp = document.getElementById("newPlantAmbientalTMP").value;
    let luminosity = document.getElementById("newPlantLuminosity").value;
    let blooming = document.getElementById("newPlantBlooming").value;
    let imgURL = document.getElementById("newPlantURL").value;
    let description = document.getElementById("newPlantDescription").value;


    //takes the name, makes it all lower case and than it changes he first letter in Uppers


    if (name.length != 0 && moisture.length != 0 && ambTemp.length != 0 && luminosity.length != 0 && blooming.length != 0 && imgURL.length != 0 && description.length != 0) {
        name = name.toLowerCase();
        let finalName = (name[0]).toUpperCase();
        let index;
        for (index = 1; index < name.length; index++) {
            finalName = finalName + name[index];
        }
        firebase.database().ref('Flowers/' + finalName).set({
            AmbientalTemp: ambTemp,
            Blooming: blooming,
            Details: description,
            ImageSource: imgURL,
            Luminosity: luminosity,
            Moisture: moisture
        });
    } else {
        alert("Please fill all the fields above");
    }
    document.getElementById("tipsntricksViewer").style.visibility = 'visible';
    document.getElementById("addNewPlantDiv").style.visibility = 'hidden';
}


/*
BRIEF:
Function to handle irrigation mode - display needed block
 */
function irrigationMode() {
    let mode = document.getElementById("irrigationMode").value;
    let selectedKey = document.getElementById("deviceList").value;
    switch (mode) {
        case "Automatic":

            document.getElementById("RecurrenceMode").style.display = "none";
            document.getElementById("AutomaticMode").style.display = "block";
            firebase.database().ref('Devices/' + selectedKey + '/IrrigationMode').update({
                IrrigationMode: "0"
            });
            break;
        case "Recurrence":
            document.getElementById("AutomaticMode").style.display = "none";
            document.getElementById("RecurrenceMode").style.display = "block";
            firebase.database().ref('Devices/' + selectedKey + '/IrrigationMode').update({
                IrrigationMode: "1"
            });
            break;
    }
}