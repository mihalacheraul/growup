var firebaseConfig = {
    apiKey: "AIzaSyD2NPGPBr7J9xPxADUwHwu01zehGHOtfvM",
    authDomain: "greenlife-4b330.firebaseapp.com",
    databaseURL: "https://greenlife-4b330.firebaseio.com",
    projectId: "greenlife-4b330",
    storageBucket: "greenlife-4b330.appspot.com",
    messagingSenderId: "903375017369",
    appId: "1:903375017369:web:dba4ae19a63460ef54af08",
    measurementId: "G-G29ZF7VF6T"
};
const auth = firebase.auth();
var database = firebase.database();
/*
BRIEF:
Function implemented in order to create a new user in DB
Called on register button
 */
function createNewUser() {
    var firstName = document.getElementById('fName').value;
    var password = document.getElementById('password').value;
    var phone = document.getElementById('phone').value;
    if (firstName.length == 0 && password.length == 0 && phone.length == 0) {
        window.alert("Please fill the fields below");
        return;
    } else if (firstName.length == 0) {
        window.alert("Please fill the first name field with your given name.");
        return;
    } else if (phone.length == 0) {
        window.alert("Please fill the phone number field with your personal phone number.");
        return;

    } else if (password.length == 0) {
        window.alert("Please fill the password field.");
        return;
    }
    if (password.length >= 6 && firstName.length != 0 && phone.lengths != 0) {
        firebase.database().ref('Users/' + phone).set({
            FirstName: firstName,
            phone: phone,
            password: password
        });
        window.location.href = 'Authentication.html';
        window.alert("You have registered successfully. Welcome!");
        return;
    } else {
        window.alert("Error: Password must be at least 6 characters!");
    }
}