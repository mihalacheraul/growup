var firebaseConfig = {
    apiKey: "AIzaSyD2NPGPBr7J9xPxADUwHwu01zehGHOtfvM",
    authDomain: "greenlife-4b330.firebaseapp.com",
    databaseURL: "https://greenlife-4b330.firebaseio.com",
    projectId: "greenlife-4b330",
    storageBucket: "greenlife-4b330.appspot.com",
    messagingSenderId: "903375017369",
    appId: "1:903375017369:web:dba4ae19a63460ef54af08",
    measurementId: "G-G29ZF7VF6T"
};
const auth = firebase.auth();
var database = firebase.database();


/*
BRIEF:
Function implemented in order to authenticate a user in appl
Called on auth button
 */
function logInButton() {
    var refUsers = database.ref("Users");
    var phone = document.getElementById("phone").value;
    var password = document.getElementById("password").value;
    var check = 0;
    refUsers.on('value', function(snapshot) {
        for (var key in snapshot.val()) {
            if (snapshot.val()[key]["phone"] == phone) {
                if (snapshot.val()[key]["password"] == password) {
                    var queryString = "?" + key;
                    window.location.href = "Appl.html" + queryString;
                    //window.alert("Log in successfully");
                    return;
                } else {
                    window.alert("Wrong password");
                    return;
                }
            }
        }
        window.alert("Phone number not found in our database");
    });
}


function registerButton() {
    window.location.href = 'Register.html';
}