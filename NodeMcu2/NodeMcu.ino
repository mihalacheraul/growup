//================================================================================================================
//                                             INCLUDES
//================================================================================================================
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <ArduinoJson.h>
#include <FirebaseArduino.h>
#include "DHT.h"
#include <NTPClient.h>
//dummy code
//================================================================================================================
//                                             DECLARATIONS
//================================================================================================================
WiFiManager wifiManager;
#define FIREBASE_HOST "greenlife-4b330.firebaseio.com"
#define FIREBASE_AUTH "yqDLeosnvoKu2olmTzANvsJXOl9Y3LT6xbf1JPGY"

//================================================================================================================
//                                             PIN MAPPING
//================================================================================================================
#define analogPin           A0      //soil moisture and water level
#define deviceConnected     16      //yellow
#define deviceNotConnected  15      //red
#define refillWaterTank     14      //red
#define waterPump           0      //relay
#define DHTTYPE             DHT11
#define dht_dpin            2
#define enablePin           14      //enable LOW
#define selectAnalogSignal  12      //MUX S0 pe S0=0 => waterLevel | S0=1 => moisture

DHT dht(dht_dpin, DHTTYPE);

char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", 3600);
//================================================================================================================
//                                             TRANSMITTED PARAMETERS
//================================================================================================================
String serialNumber = "z7Vpi5yW";   //unique Identifier

//================================================================================================================
//                                             SETUP
//================================================================================================================
void setup() {
  Serial.begin(115200);
  pinSet();
  ConnectAPMode();
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  dht.begin();
  timeClient.begin();
}

//================================================================================================================
//                                             Access Point MODE
//================================================================================================================
void ConnectAPMode()
{
  Serial.println("Connecting to GrowUP");
  wifiManager.autoConnect("GrowUP");
  if (!wifiManager.autoConnect()) {
    Serial.println("Failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    digitalWrite(deviceNotConnected, HIGH);
    digitalWrite(deviceConnected, LOW);
    ESP.reset();
    delay(1000);

  }
  else
  {
    digitalWrite(deviceNotConnected, LOW);
    digitalWrite(deviceConnected, HIGH);
  }
}

//================================================================================================================
//                                             SETTING PIN MODE
//================================================================================================================
void pinSet()
{
  pinMode(analogPin, INPUT);
  pinMode(deviceConnected, OUTPUT);
  pinMode(deviceNotConnected, OUTPUT);
  pinMode(refillWaterTank, OUTPUT);
  pinMode(waterPump, OUTPUT);
  pinMode(enablePin, OUTPUT);
  pinMode(selectAnalogSignal, OUTPUT);

  digitalWrite(enablePin, LOW); //enables the MUX
}

//================================================================================================================
//                                             MAIN LOOP
//================================================================================================================
void loop() {
  getDataFromFirebase();
  delay(1000);
}

//================================================================================================================
//                                             SET DATA TO FIREBASE
//================================================================================================================
void setDataToFirebase()
{
  Firebase.setFloat("Devices/" + serialNumber + "/Air temperature", readRooomTemperature());
  Firebase.setFloat("Devices/" + serialNumber + "/Soil moisture", readMoistureValue());
  Firebase.setFloat("Devices/" + serialNumber + "/Water tank level", readWaterLevelValue());
}

//================================================================================================================
//                                             GET DATA FROM FIREBASE
//================================================================================================================
void getDataFromFirebase()
{
  setDataToFirebase();
  int option = 0;
  option = (Firebase.getString("Devices/" + serialNumber + "/IrrigationMode/IrrigationMode")).toInt();

  switch (option)
  {
    case 0:
      enableAutomaticMode();
      break;
    case 1:
      enableRecurrentMode();
      break;
    default:    //default, Device NOT connected to internet
      digitalWrite(deviceNotConnected, HIGH);
      digitalWrite(deviceConnected, LOW);
  }
}

//**************************************************READ METHODS**************************************************

//================================================================================================================
//                                             READ Room Temperature
//================================================================================================================
float readRooomTemperature()
{
  float fAirTemperature = dht.readTemperature();
  fAirTemperature = (int)(fAirTemperature * 10);
  fAirTemperature = fAirTemperature / 10;
  Serial.println(fAirTemperature);
  return fAirTemperature;
}

//================================================================================================================
//                                             READ Moisture Value
//================================================================================================================
float readMoistureValue()
{
  digitalWrite(selectAnalogSignal, LOW);
  float fMoistureValue;
  fMoistureValue = analogRead(analogPin);
  Serial.println("moisture: ");
  Serial.println(fMoistureValue);

  fMoistureValue = 0.24 * fMoistureValue - 85.5;
  if (fMoistureValue < 0) //safety check for parameters
    fMoistureValue = 0;
  if (fMoistureValue > 100)
    fMoistureValue = 100;
  fMoistureValue = fMoistureValue * 10;
  fMoistureValue = (int) fMoistureValue;
  fMoistureValue = fMoistureValue / 10;
  Firebase.setFloat("Devices/" + serialNumber + "/Soil moisture", fMoistureValue);
  return fMoistureValue;
}

//================================================================================================================
//                                             READ WaterLevel Value
//================================================================================================================
float readWaterLevelValue()
{
  digitalWrite(selectAnalogSignal, HIGH);
  float fTankLevel = analogRead(analogPin);
  Serial.println("level: ");
  Serial.println(fTankLevel);
  fTankLevel = (fTankLevel - 690) * 0.299;
  if (fTankLevel < 0) //safety check for parameters
    fTankLevel = 0;
  if (fTankLevel > 100)
    fTankLevel = 100;
  return fTankLevel;
}

//================================================================================================================
//                                             AUTOMATIC MODE
//================================================================================================================
void enableAutomaticMode()
{
  Serial.println("automatic");
  int getSoilMoisture = (Firebase.getString("Devices/" + serialNumber + "/IrrigationMode/AutomaticQuantity")).toInt();
  Serial.println(getSoilMoisture);

  if (getSoilMoisture > 90)       // to avoid making humidity >100%
    getSoilMoisture = 90;
  if (getSoilMoisture < 10)       // to avoid going below <0%
    getSoilMoisture = 10;
  int val = readMoistureValue();
  if (val < getSoilMoisture - 10)                                                                     //moisture of soil smaller than set moisture
  {
    Serial.println("Automatic: Pump ON");
    while (val <= getSoilMoisture + 5)                                 //moisture of soil smaller than set moisture and there is enough water in tank
    {
      pumpWaterON();
      val = readMoistureValue();
      Serial.println(readMoistureValue());
      Serial.println(getSoilMoisture);

      //if users changes rules while irrigation is on, it changes automatically
      getSoilMoisture = (Firebase.getString("Devices/" + serialNumber + "/IrrigationMode/AutomaticQuantity")).toInt();
      if ((Firebase.getString("Devices/" + serialNumber + "/IrrigationMode/IrrigationMode")).toInt() == '2')
        enableRecurrentMode();
    }
    setDataToFirebase();
  }
  pumpWaterOFF();
  Serial.println("Automatic: Pump OFF");
}

//================================================================================================================
//                                             RECURRENT MODE
//================================================================================================================
void enableRecurrentMode()
{
  Serial.println("recurrence");
  timeClient.update();
  String currentDay, currentHour, currentMinutes;

  currentDay = daysOfTheWeek[timeClient.getDay()];

  if (Firebase.getString("Devices/" + serialNumber + "/weekDays/" + currentDay) == "1")
  {
    int getTimeOfTheDay = (Firebase.getString("Devices/" + serialNumber + "/IrrigationMode/RecurrenceTOD")).toInt();    //1=morning | 2=midday | 3=evening | 4=afternoon
    switch (getTimeOfTheDay)
    {
      case '1': //morning
        if ((int)timeClient.getHours() == 9)
        {
          if ((int)timeClient.getMinutes() == 0)
          {
            int getRecurrentQuantity = (Firebase.getString("Devices/" + serialNumber + "/IrrigationMode/RecurrencyQuantity")).toInt();
            irrigationOnQuantity(getRecurrentQuantity);
            while (timeClient.getMinutes() == 0) {}
          }
        }
        break;
      case '2': //midday
        if ((int)timeClient.getHours() == 12)
        {
          if ((int)timeClient.getMinutes() == 0)
          {
            int getRecurrentQuantity = (Firebase.getString("Devices/" + serialNumber + "/IrrigationMode/RecurrencyQuantity")).toInt();
            irrigationOnQuantity(getRecurrentQuantity);
            while (timeClient.getMinutes() == 0) {}
          }
        }
        break;
      case '3': //evening
        if ((int)timeClient.getHours() == 15)
        {
          if ((int)timeClient.getMinutes() == 0)
          {
            int getRecurrentQuantity = (Firebase.getString("Devices/" + serialNumber + "/IrrigationMode/RecurrencyQuantity")).toInt();
            irrigationOnQuantity(getRecurrentQuantity);
            while (timeClient.getMinutes() == 0) {}
          }
        }
        break;
      case '4': //afternoon
        if ((int)timeClient.getHours() == 19)
        {
          if ((int)timeClient.getMinutes() == 0)
          {
            int getRecurrentQuantity = (Firebase.getString("Devices/" + serialNumber + "/IrrigationMode/RecurrencyQuantity")).toInt();
            irrigationOnQuantity(getRecurrentQuantity);
            while (timeClient.getMinutes() == 0) {}
          }
        }
        break;
    }
  }
  else
  {
    Serial.println("Recurrence: " + currentDay + " is not active");
  }
  int manualPump = (Firebase.getString("Devices/" + serialNumber + "/IrrigationMode/ManualButton")).toInt();
  if (manualPump == 1)
  {
    int manualQuantity = (Firebase.getString("Devices/" + serialNumber + "/IrrigationMode/RecurrencyQuantity")).toInt();
    irrigationOnQuantity(manualQuantity);
    Firebase.setString("Devices/" + serialNumber + "/IrrigationMode/ManualButton", "0");
    setDataToFirebase();
  }
}

void irrigationOnQuantity(int quantity)
{
  float mlToPercent = quantity / 100 * 16.6; //ml to percent
  float currentWaterLevel = readWaterLevelValue();
  float necessaryWaterLevel = currentWaterLevel - mlToPercent;
  while (currentWaterLevel != necessaryWaterLevel)
  {
    pumpWaterON();
    currentWaterLevel = readWaterLevelValue();
    setDataToFirebase();
  }
  pumpWaterOFF();
  setDataToFirebase();
}

void pumpWaterON()
{
  if (readWaterLevelValue() > 0)
    digitalWrite(waterPump, LOW); //relay ON
  else
    digitalWrite(waterPump, HIGH); //relay OFF

}
void pumpWaterOFF()
{
  digitalWrite(waterPump, HIGH); //relay OFF
}
